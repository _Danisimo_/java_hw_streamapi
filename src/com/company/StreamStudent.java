package com.company;

import java.util.List;
import java.util.stream.Collectors;

public class StreamStudent {
    static List<Student> studentsFaculty(List<Student> students, String faculty) {
        return students.stream().filter((a) -> a.getFaculty()
                .equals(faculty))
                .collect(Collectors.toList());
    }

    static List<Student> studentsFacultyAndCourse(List<Student> students, String faculty, int course) {
        return students.stream().filter((a) -> a.getFaculty()
                .equals(faculty) && a.getCourse() == course)
                .collect(Collectors.toList());
    }

    static List<Student> studentsYearOfBirth(List<Student> students, int year) {
        return students.stream().filter((a) -> a.getYearOfBirth() >= year)
                .collect(Collectors.toList());
    }

    static List<String> studentsByGroup(List<Student> students, String group) {

        return students.stream().filter((a) -> a.getGroup().equals(group))
                .map(a -> a.getFirstName() + ", " + a.getLastName() + ", " + a.getPatronymic())
                .collect(Collectors.toList());
    }

    public static List<String> studentsByNameInGroup(List<Student> students) {
        return students.stream().map(student -> student.getLastName() + " "
                + student.getFirstName() + " " + student.getPatronymic())
                .collect(Collectors.toList());
    }

    public static long studentsByCountInFaculty(List<Student> students, String faculty) {
        return students.stream().filter(student -> student.getFaculty()
                .equals(faculty)).count();
    }
}
