package com.company;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;


public class MainTest {

    List<Student> students = new ArrayList<>();

    @Test
    public void studentsFacultyTest(){
        List<Student> test = new ArrayList<Student>();
        List<Student> actual = new ArrayList<Student>();

        Student student = new Student (1, "Ulius", "Monochrona", "Strimovych", 1990, "CleverKingdom", "0123456789", "Magic", 5, "MG-17");

        actual.add(student);
        test.add(student);

        assertEquals(test,StreamStudent.studentsFaculty(actual,"Magic"));
    }

    @Test
    public void studentsFacultyAndCourseTest(){
        List<Student> test = new ArrayList<Student>();
        List<Student> actual = new ArrayList<Student>();

        Student student = new Student (1, "Ulius", "Monochrona", "Strimovych", 1990, "CleverKingdom", "0123456789", "Magic", 5, "MG-17");

        actual.add(student);
        test.add(student);

        assertEquals(test,StreamStudent.studentsFacultyAndCourse(actual,"Magic" , 5));
    }

    @Test
    public void studentsYearOfBirthTest(){
        List<Student> test  = new ArrayList<Student>();
        List<Student> actual = new ArrayList<Student>();

        Student student = new Student (1, "Ulius", "Monochrona", "Strimovych", 1990, "CleverKingdom", "0123456789", "Magic", 5, "MG-17");

        actual.add(student);
        test.add(student);

        assertEquals(test,StreamStudent.studentsYearOfBirth(actual,1990));
    }

    @Test
    public void studentByGroupTest(){
        List<Student> test = new ArrayList<Student>();
        List<Student> actual = new ArrayList<Student>();

        Student student = new Student (1, "Ulius", "Monochrona", "Strimovych", 1990, "CleverKingdom", "0123456789", "Magic", 5, "MG-17");

        actual.add(student);
        test.add(student);

        assertEquals(test,StreamStudent.studentsByGroup(actual,"MG-17"));
    }

    @Test
    public void studentsByNameInGroupTest(){
        List<Student> test = new ArrayList<Student>();
        List<Student> actual = new ArrayList<Student>();

        Student student = new Student (1, "Ulius", "Monochrona", "Strimovych", 1990, "CleverKingdom", "0123456789", "Magic", 5, "MG-17");

        actual.add(student);
        test.add(student);

        assertEquals(test,StreamStudent.studentsByNameInGroup(actual));

    }

    @Test
    public void studentByCountInFacultyTest(){
        List<Student> test = new ArrayList<Student>();
        List<Student> actual = new ArrayList<Student>();

        Student student = new Student (1, "Ulius", "Monochrona", "Strimovych", 1990, "CleverKingdom", "0123456789", "Magic", 5, "MG-17");

        actual.add(student);
        test.add(student);

        assertEquals(test,StreamStudent.studentsByCountInFaculty(actual,"Magic"));
    }

}
