package com.company;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        List<Student> students = new ArrayList<Student>();

        students.add(new Student(1, "Ulius", "Monochrona", "Strimovych", 1990, "CleverKingdom", "0123456789", "Magic", 5, "MG-17"));
        students.add(new Student(2, "Asta", "Black", "Alistovich", 2002, "Hardge", "0321654987", "Strenge", 3, "MMA-1"));
        students.add(new Student(3, "Yuno", "Black", "Alistovich", 2002, "Hardge", "0321654988", "Magic", 3, "MG - 21"));
        students.add(new Student(44, "Noel", "Silva", "Ilistovna", 1999, "Donbas", "14881588", "Support", 6, "SUP - 12"));


    }

}